﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace CustomArray
{
    public class CustomArray<T> : IEnumerable<T>
    {
        private readonly int first;
        private int length;


        /// <summary>
        /// Should return first index of array
        /// </summary>
        public int First
        {
            get => first;
            private set => throw new NotImplementedException();
        }

        /// <summary>
        /// Should return last index of array
        /// </summary>
        public int Last
        {
            get => first+length-1;
        }

        /// <summary>
        /// Should return length of array
        /// <exception cref="ArgumentException">Thrown when value was smaller than 0</exception>
        /// </summary>
        public int Length
        {
            get => length;
            private set
            {
                if (value<=0)
                {
                    throw new ArgumentException(value.ToString());
                }
                else
                {
                    length = value;
                }
            }
        }

        /// <summary>
        /// Should return array 
        /// </summary>
        public T[] Array { get; }


        /// <summary>
        /// Constructor with first index and length
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="length">Length</param>         
        public CustomArray(int first, int length)
        {

            this.first = first;
            Length = length;
            Array = new T[length];
        }


        /// <summary>
        /// Constructor with first index and collection  
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Collection</param>
        ///  <exception cref="NullReferenceException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when count is smaler than 0</exception>
        public CustomArray(int first, IEnumerable<T> list)
        {
            if (list is null)
            {
                throw (new ArgumentException("list") as SystemException) as NullReferenceException;
            }

            if (list.Count() < 0)
            {
                throw new ArgumentException("list");
            }

            this.first = first;
            this.Array = Enumerable.ToArray<T>(list);
            Length = Array.Length;
        }

        /// <summary>
        /// Constructor with first index and params
        /// </summary>
        /// <param name="first">First Index</param>
        /// <param name="list">Params</param>
        ///  <exception cref="ArgumentNullException">Thrown when list is null</exception>
        /// <exception cref="ArgumentException">Thrown when list without elements </exception>
        public CustomArray(int first, params T[] list)
        {
            if (list.Count() < 0)
            {
                throw new ArgumentException("list");
            }

            this.first = first;
            this.Array = list;
            Length = Array.Length;
        }

        /// <summary>
        /// Indexer with get and set  
        /// </summary>
        /// <param name="item">Int index</param>        
        /// <returns></returns>
        /// <exception cref="ArgumentException">Thrown when index out of array range</exception> 
        /// <exception cref="ArgumentNullException">Thrown in set  when value passed in indexer is null</exception>
        public T this[int item]
        {

            get
            {
                if (item > Last || item < First)
                {
                    throw new ArgumentException("item");
                }
                if (Array[item - first] is null)
                {
                    throw new ArgumentNullException("item");
                }
                return Array[item - first];
            }
            set
            {
                if (item > Last || item < First)
                {
                    throw new ArgumentException("item");
                }
                if (value is null)
                {
                    throw new ArgumentNullException("value");
                }
                Array[item - first] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new TEnumerator<T>(Array);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)Array).GetEnumerator();
        }
    }

    internal class TEnumerator<T> : IEnumerator<T>
    {
        private T[] Ts;
        private int position = -1;
        public TEnumerator(T[] Ts)
        {
            this.Ts = Ts;
        }

        public T Current
        {
            get
            {
                if (position == -1 || position >= Ts.Length)
                {
                    throw new InvalidOperationException();
                }

                return Ts[position];
            }
        }

        private object Current1
        {
            get { return this.Current; }
        }

        object IEnumerator.Current
        {
            get { return Current1; }
        }


        public bool MoveNext()
        {
            if (position < Ts.Length - 1)
            {
                position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            position = -1;
        }

        // Implement IDisposable, which is also implemented by IEnumerator(T).
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue && disposing)
            {
                position = -1;
                Ts = null;
            }

            disposedValue = true;
        }

        ~TEnumerator()
        {
            Dispose(false);
        }
    }

}
